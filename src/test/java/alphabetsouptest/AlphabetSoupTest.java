package alphabetsouptest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import alphabetsoup.AlphabetSoup;

class AlphabetSoupTest
{
	@Test
	void testAlphabetSoupTwoByTwo()
	{
		AlphabetSoup alphabetSoup;
		int rowCount = 2;
		int columnCount = 2;
		String s = "AAAA";
		char[][] buffer = makeMyBuffer(rowCount, columnCount, s);	
		
		alphabetSoup = new AlphabetSoup(rowCount, columnCount, buffer);
		alphabetSoup.construct();
		ArrayList<String> wordsToFind = new ArrayList<String>();
		wordsToFind.add("AA");

		String expectedOutput = "AA 0:0 0:1\n"
				+ "AA 1:0 1:1\n"
				+ "AA 0:1 0:0\n"
				+ "AA 1:1 1:0\n"
				+ "AA 0:0 1:0\n"
				+ "AA 0:1 1:1\n"
				+ "AA 1:0 0:0\n"
				+ "AA 1:1 0:1\n"
				+ "AA 0:0 1:1\n"
				+ "AA 1:0 0:1\n"
				+ "AA 0:1 1:0\n"
				+ "AA 1:1 0:0\n";
		String actualOutput = alphabetSoup.search(wordsToFind);		
		assertEquals(expectedOutput, actualOutput);
	}
	
	@Test
	void testAlphabetSoupTwoByTwoPartTwo()
	{
		AlphabetSoup alphabetSoup;
		int rowCount = 2;
		int columnCount = 2;
		String s = "HIHI";
		char[][] buffer = makeMyBuffer(rowCount, columnCount, s);	
		
		alphabetSoup = new AlphabetSoup(rowCount, columnCount, buffer);
		alphabetSoup.construct();
		ArrayList<String> wordsToFind = new ArrayList<String>();
		wordsToFind.add("HI");

		String expectedOutput = "HI 0:0 0:1\n"
				+ "HI 1:0 1:1\n"
				+ "HI 0:0 1:1\n"
				+ "HI 1:0 0:1\n";
		String actualOutput = alphabetSoup.search(wordsToFind);		
		assertEquals(expectedOutput, actualOutput);
	}
	
	@Test
	void testAlphabetSoupEmpty()
	{
		AlphabetSoup alphabetSoup;
		int rowCount = 0;
		int columnCount = 0;
		String s = "";
		char[][] buffer = makeMyBuffer(rowCount, columnCount, s);	
					
		alphabetSoup = new AlphabetSoup(rowCount, columnCount, buffer);
		alphabetSoup.construct();
		ArrayList<String> wordsToFind = new ArrayList<String>();
		wordsToFind.add("HELLO");

		String expectedOutput = "";
		String actualOutput = alphabetSoup.search(wordsToFind);
		assertEquals(expectedOutput, actualOutput);
	}

	@Test
	void testAlphabetSoupOneColumn()
	{
		AlphabetSoup alphabetSoup;
		int rowCount = 7;
		int columnCount = 1;
		String s = "XHELLOX";
		char[][] buffer = makeMyBuffer(rowCount, columnCount, s);	
		
		alphabetSoup = new AlphabetSoup(rowCount, columnCount, buffer);
		alphabetSoup.construct();
		ArrayList<String> wordsToFind = new ArrayList<String>();
		wordsToFind.add("HELLO");
		
		String expectedOutput = "HELLO 1:0 5:0\n";
		String actualOutput = alphabetSoup.search(wordsToFind);
		assertEquals(expectedOutput, actualOutput);
	}
	
	@Test
	void testAlphabetSoupOneRow()
	{
		AlphabetSoup alphabetSoup;
		int rowCount = 1;
		int columnCount = 7;
		String s = "XHELLOX";
		char[][] buffer = makeMyBuffer(rowCount, columnCount, s);	
		
		alphabetSoup = new AlphabetSoup(rowCount, columnCount, buffer);
		alphabetSoup.construct();
		ArrayList<String> wordsToFind = new ArrayList<String>();
		wordsToFind.add("HELLO");

		String expectedOutput = "HELLO 0:1 0:5\n";
		String actualOutput = alphabetSoup.search(wordsToFind);;
		assertEquals(expectedOutput, actualOutput);
	}

	@Test
	void testAlphabetSoupNoHits()
	{
		AlphabetSoup alphabetSoup;
		int rowCount = 2;
		int columnCount = 2;
		String s = "AAAA";
		char[][] buffer = makeMyBuffer(rowCount, columnCount, s);	
		
		alphabetSoup = new AlphabetSoup(rowCount, columnCount, buffer);
		alphabetSoup.construct();
		ArrayList<String> wordsToFind = new ArrayList<String>();
		wordsToFind.add("BB");

		String expectedOutput = "";
		String actualOutput = alphabetSoup.search(wordsToFind);;
		assertEquals(expectedOutput, actualOutput);
	}
	
	@Test
	void testAlphabetSoupLong()
	{
		AlphabetSoup alphabetSoup;
		int rowCount;
		int columnCount;
		char[][] buffer;
		String s = "OnceuponamidnightdrearywhileIponderedweakandwearyOvermanyaquaintandcuriousvolumeofforgottenloreWhileInoddednearlynappingsuddenlytherecameatappingAsofsomeonegentlyrappingrappingatmychamberdoorTissomevisitorImutteredtappingatmychamberdoorOnlythisandnothingmoreAhdistinctlyIrememberitwasinthebleakDecemberAndeachseparatedyingemberwroughtitsghostuponthefloorEagerlyIwishedthemorrowvainlyIhadsoughttoborrowFrommybookssurceaseofsorrowsorrowforthelostLenoreFortherareandradiantmaidenwhomtheangelsnameLenoreNamelesshereforevermoreAndthesilkensaduncertainrustlingofeachpurplecurtainThrilledmefilledmewithfantasticterrorsneverfeltbeforeSothatnowtostillthebeatingofmyheartIstoodrepeatingTissomevisitorentreatingentranceatmychamberdoorSomelatevisitorentreatingentranceatmychamberdoorThisitisandnothingmorePresentlymysoulgrewstrongerhesitatingthennolongerSirsaidIorMadamtrulyyourforgivenessIimploreButthefactisIwasnappingandsogentlyyoucamerappingAndsofaintlyyoucametappingtappingatmychamberdoorThatIscarcewassureIheardyouhereIopenedwidethedoorDarknessthereandnothingmoreDeepintothatdarknesspeeringlongIstoodtherewonderingfearingDoubtingdreamingdreamsnomortalseverdaredtodreambeforeButthesilencewasunbrokenandthestillnessgavenotokenAndtheonlywordtherespokenwasthewhisperedwordLenoreThisIwhisperedandanechomurmuredbackthewordLenoreMerelythisandnothingmoreBackintothechamberturningallmysoulwithinmeburningSoonagainIheardatappingsomewhatlouderthanbeforeSurelysaidIsurelythatissomethingatmywindowlatticeLetmeseethenwhatthereatisandthismysteryexploreLetmyheartbestillamomentandthismysteryexploreTisthewindandnothingmoreOpenhereIflungtheshutterwhenwithmanyaflirtandflutterIntheresteppedastatelyravenofthesaintlydaysofyoreNottheleastobeisancemadehenotaminutestoppedorstayedheButwithmienoflordorladyperchedabovemychamberdoorPercheduponabustofPallasjustabovemychamberdoorPerchedandsatandnothingmoreThenthisebonybirdbeguilingmysadfancyintosmilingBythegraveandsterndecorumofthecountenanceitworeThoughthycrestbeshornandshaventhouIsaidartsurenocravenGhastlygrimandancientravenwanderingfromtheNightlyshoreTellmewhatthylordlynameisontheNightsPlutonianshoreQuoththeRavenNevermoreMuchImarvelledthisungainlyfowltoheardiscoursesoplainlyThoughitsanswerlittlemeaninglittlerelevancyboreForwecannothelpagreeingthatnolivinghumanbeingEveryetwasblestwithseeingbirdabovehischamberdoorBirdorbeastuponthesculpturedbustabovehischamberdoorWithsuchnameasNevermoreButtheRavensittinglonelyontheplacidbustspokeonlyThatonewordasifhissoulinthatonewordhedidoutpourNothingfurtherthenheutterednotafeatherthenheflutteredTillIscarcelymorethanmutteredotherfriendshaveflownbeforeOnthemorrowhewillleavemeasmyhopeshaveflownbeforeThenthebirdsaidNevermoreStartledatthestillnessbrokenbyreplysoaptlyspokenDoubtlesssaidIwhatituttersisitsonlystockandstoreCaughtfromsomeunhappymasterwhomunmercifulDisasterFollowedfastandfollowedfastertillhissongsoneburdenboreTillthedirgesofhisHopethatmelancholyburdenboreOfNevernevermoreButtheRavenstillbeguilingmysadfancyintosmilingStraightIwheeledacushionedseatinfrontofbirdandbustanddoorThenuponthevelvetsinkingIbetookmyselftolinkingFancyuntofancythinkingwhatthisominousbirdofyoreWhatthisgrimungainlyghastlygauntandominousbirdofyoreMeantincroakingNevermoreThisIsatengagedinguessingbutnosyllableexpressingTothefowlwhosefieryeyesnowburnedintomybosomscoreThisandmoreIsatdiviningwithmyheadateaserecliningOnthecushionsvelvetliningthatthelamplightgloatedoerButwhosevelvetvioletliningwiththelamplightgloatingoerSheshallpressahnevermoreThenmethoughttheairgrewdenserperfumedfromanunseencenserSwungbySeraphimwhosefootfallstinkledonthetuftedfloorWretchIcriedthyGodhathlenttheebytheseangelshehathsenttheeRespiterespiteandnepenthefromthymemoriesofLenoreQuaffohquaffthiskindnepentheandforgetthislostLenoreQuoththeRavenNevermoreProphetsaidIthingofevilprophetstillifbirdordevilWhetherTemptersentorwhethertempesttossedtheehereashoreDesolateyetallundauntedonthisdesertlandenchantedOnthishomebyhorrorhauntedtellmetrulyIimploreIsthereistherebalminGileadtellmetellmeIimploreQuoththeRavenNevermoreProphetsaidIthingofevilprophetstillifbirdordevilBythatHeaventhatbendsaboveusbythatGodwebothadoreTellthissoulwithsorrowladenifwithinthedistantAidennItshallclaspasaintedmaidenwhomtheangelsnameLenoreClasparareandradiantmaidenwhomtheangelsnameLenoreQuoththeRavenNevermoreBethatwordoursigninpartingbirdorfiendIshriekedupstartingGettheebackintothetempestandtheNightsPlutonianshoreLeavenoblackplumeasatokenofthatliethysoulhathspokenLeavemylonelinessunbrokenquitthebustabovemydoorTakethybeakfromoutmyheartandtakethyformfromoffmydoorQuoththeRavenNevermoreAndtheRavenneverflittingstillissittingstillissittingOnthepallidbustofPallasjustabovemychamberdoorAndhiseyeshavealltheseemingofademonsthatisdreamingAndthelamplightoerhimstreamingthrowshisshadowonthefloorAndmysoulfromoutthatshadowthatliesfloatingonthefloorShallbeliftednevermore";			
		s = s.toUpperCase();

		columnCount = 20;
		rowCount = s.length() / 20; // end up truncating the data
		buffer = makeMyBuffer(rowCount, columnCount, s);
		
		alphabetSoup = new AlphabetSoup(rowCount, columnCount, buffer);
		alphabetSoup.construct();
		ArrayList<String> wordsToFind = new ArrayList<String>();
		wordsToFind.add("NEVER");
		wordsToFind.add("RAVEN");
		wordsToFind.add("LENORE");
		
		String expectedOutput = "NEVER 30:11 30:15\n"
				+ "NEVER 107:0 107:4\n"
				+ "NEVER 120:8 120:12\n"
				+ "NEVER 134:4 134:8\n"
				+ "NEVER 147:0 147:4\n"
				+ "NEVER 147:5 147:9\n"
				+ "NEVER 174:9 174:13\n"
				+ "NEVER 188:14 188:18\n"
				+ "NEVER 215:3 215:7\n"
				+ "NEVER 229:2 229:6\n"
				+ "NEVER 230:2 230:6\n"
				+ "RAVEN 83:14 83:18\n"
				+ "RAVEN 102:4 102:8\n"
				+ "RAVEN 106:15 106:19\n"
				+ "RAVEN 121:3 121:7\n"
				+ "RAVEN 148:0 148:4\n"
				+ "RAVEN 188:9 188:13\n"
				+ "RAVEN 201:11 201:15\n"
				+ "LENORE 22:4 22:9\n"
				+ "LENORE 24:13 24:18\n"
				+ "LENORE 63:2 63:7\n"
				+ "LENORE 65:10 65:15\n"
				+ "LENORE 185:4 185:9\n"
				+ "LENORE 214:4 214:9\n";
		String actualOutput = alphabetSoup.search(wordsToFind);
		
		assertEquals(expectedOutput, actualOutput);
	}
	
	char[][] makeMyBuffer(int rowCount, int columnCount, String s)
	{
		char[][] buffer = new char[rowCount][columnCount];
		
		for(int i = 0; i < rowCount; i++)
		{
			buffer[i] = (s.substring(0, columnCount)).toCharArray();			
			s = s.substring(columnCount);			
		}

		return buffer;
	}
}
