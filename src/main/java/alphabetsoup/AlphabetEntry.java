package alphabetsoup;

import java.util.List;

public class AlphabetEntry
{		
	private String alphabetSoupString;
	private List<RowColumn> indices;

	public String getAlphabetSoupString()
	{
		return alphabetSoupString;
	}
	
	public void setAlphabetSoupString(String s)
	{
		alphabetSoupString = s;
	}
	
	public void setIndices(List<RowColumn> rc)
	{
		indices = rc;
	}
	
	public List<RowColumn> getIndices()
	{
		return indices;
	}
	
	public void setRowColumn()
	{
		
	}
}
