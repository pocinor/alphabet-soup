/**
 * 
 */
package alphabetsoup;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/*
  	Welcome to AlphabetSoup.  The goal is to find strings with a grid of letters.  The strategy I took was to 
  	find all possible string permutations in the grid store them in a list.  Along with those strings, there is an
  	array indicating the row and column of each character.  While it is easy enough calculate the indices for a 
  	horizontal or vertical entry, it  starts to get trickier with the diagonal entries.  This also makes the code
  	easier to understand and read.
  	
  	 Nothing in the README states whether there can multiple entries in the grid per search word, so I accounted for this
  	 by searching through the entire list.  It also wasn't stated if we could have multiple entries *per line*.  I
  	 accounted for this by creating a substring of the remaining string once we get a hit.
 */
public class AlphabetSoup
{	
	private static char[][] alphabetSoup;
	private static int rowCount;
	private static int columnCount;
	private static List<AlphabetEntry> alphabetList;

	/**
	 * 
	 */
	public AlphabetSoup(int rowCount, int columnCount, char[][] alphabetSoup)
	{
		this.rowCount = rowCount;
		this.columnCount = columnCount;
		this.alphabetSoup = alphabetSoup;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		if(args.length != 1)
		{
			System.err.println("AlphabetSoup [path to file]");
			return;
		}
		
		File inputFile = new File(args[0]);
		
		if(inputFile.exists() == false && inputFile.canRead() == false)
		{
			System.err.println("Unable to open input file " + inputFile);
			return;
		}

		long length = inputFile.length();
		char[] buffer = new char[(int) length];
		
		try
		{
			FileReader inputFileReader = new FileReader(inputFile);
			int bytesRead = inputFileReader.read(buffer);
			
			if(bytesRead != length)
			{
				System.err.println("Unable to read full data file.");
				return;
			}
			
			inputFileReader.close();
		}

		catch(Exception e)
		{
			System.err.println("Caught exception opening file " + inputFile.getName() + e.getMessage());
			return;
		}
		
		int i = 0;
		ArrayList<String> wordsToFind = new ArrayList<String>();
		
		// normalize the string and then split along the newlines
		for(String s : ((new String(buffer)).toUpperCase()).split("\n"))
		{
			// assuming the file is correctly formatted, the first line represents rows by columns
			if(i == 0)
			{
				String[] splitDimensions = s.split("X");
				rowCount = Integer.parseInt(splitDimensions[0]);
				columnCount = Integer.parseInt(splitDimensions[1]);
				alphabetSoup = new char[rowCount][columnCount];				
			}
			
			// the next set of rows represents the characters of the word search
			else if(i <= rowCount)
			{
				// normalize by removing spaces
				alphabetSoup[i - 1] = (s.replaceAll(" ", "")).toCharArray();
			}
			
			// the remaining data represents the words to search for in the list
			else
			{
				wordsToFind.add(s.replaceAll(" ", ""));
			}
			
			i++;
		}
		
		construct();
		System.out.println(search(wordsToFind));
	}

	static public String search(ArrayList<String> wordsToFind)
	{
		String returnString = "";
		
		// search all permutations in case we have multiple hits
		for(String s : wordsToFind)
		{
			for(AlphabetEntry entry : alphabetList)
			{
				int startingFrom = 0;
				int index = -1;
				
				// must search repeatedly in case we have multiple hits per line
				while((index = entry.getAlphabetSoupString().indexOf(s, startingFrom)) != -1)
				{
					RowColumn startingRc = entry.getIndices().get(index);
					RowColumn endingRc = entry.getIndices().get(index + s.length() - 1);
					
					returnString = returnString + 
							   s + " " + 
						       startingRc.getRow() + ":" + startingRc.getColumn() + " " +  
						       endingRc.getRow() + ":" + endingRc.getColumn() + '\n';

					startingFrom = index + 1;
				}
			}
		}
		
		return returnString;
	}

	public static void construct()
	{
		alphabetList = new ArrayList<AlphabetEntry>();
				
		// preconstruct all possible string permutations
		constructEast();
		constructWest();
		constructSouth();
		constructNorth();
		constructSoutheast();
		constructNortheast(); 
		constructSouthwest();
		constructNorthwest();		
	}
	
	// strings go from left to right
	private static void constructEast()
	{
		for(int i = 0; i < rowCount; i++)
		{
			AlphabetEntry entry = new AlphabetEntry();
			entry.setAlphabetSoupString("");
			entry.setIndices(new ArrayList<RowColumn>());
			
			for(int j = 0; j < columnCount; j++)
			{
				entry.setAlphabetSoupString(entry.getAlphabetSoupString() + alphabetSoup[i][j]);
				(entry.getIndices()).add(new RowColumn(i, j));
			}
			
			alphabetList.add(entry);
		}
	}

	// strings go from right to left
	private static void constructWest()
	{
		for(int i = 0; i < rowCount; i++)
		{
			AlphabetEntry entry = new AlphabetEntry();
			entry.setAlphabetSoupString("");
			entry.setIndices(new ArrayList<RowColumn>());
			
			for(int j = columnCount - 1; j >= 0; j--)
			{
				entry.setAlphabetSoupString(entry.getAlphabetSoupString() + alphabetSoup[i][j]);
				(entry.getIndices()).add(new RowColumn(i, j));
			}
			
			alphabetList.add(entry);
		}
	}

	// strings go from top to bottom
	private static void constructSouth()
	{	
		// flip rows and columns
		for(int i = 0; i < columnCount; i++)
		{
			AlphabetEntry entry = new AlphabetEntry();
			entry.setAlphabetSoupString("");
			entry.setIndices(new ArrayList<RowColumn>());
			
			for(int j = 0; j < rowCount; j++)
			{
				entry.setAlphabetSoupString(entry.getAlphabetSoupString() + alphabetSoup[j][i]);
				(entry.getIndices()).add(new RowColumn(j, i));
			}
			
			alphabetList.add(entry);
		}		
	}
	
	// strings go from bottom to top
	private static void constructNorth()
	{
		// flip rows and columns
		for(int i = 0; i < columnCount; i++)
		{
			AlphabetEntry entry = new AlphabetEntry();
			entry.setAlphabetSoupString("");
			entry.setIndices(new ArrayList<RowColumn>());
			
			for(int j = rowCount - 1; j >= 0; j--)
			{
				entry.setAlphabetSoupString(entry.getAlphabetSoupString() + alphabetSoup[j][i]);
				(entry.getIndices()).add(new RowColumn(j, i));
			}
			
			alphabetList.add(entry);
		}		
	}

	// strings go from top left to lower right
	private static void constructSoutheast()
	{		
		// upper matrix		
		for(int outerColumnCounter = columnCount - 1; outerColumnCounter >= 0; outerColumnCounter--)
		{
			AlphabetEntry entry = new AlphabetEntry();
			entry.setAlphabetSoupString("");
			entry.setIndices(new ArrayList<RowColumn>());
						
			for(int rowCounter = 0, innerColumnCounter = outerColumnCounter; 
				rowCounter < rowCount && innerColumnCounter < columnCount;
				rowCounter++, innerColumnCounter++)
			{
				entry.setAlphabetSoupString( entry.getAlphabetSoupString() + alphabetSoup[rowCounter][innerColumnCounter]);
				(entry.getIndices()).add(new RowColumn(rowCounter, innerColumnCounter));				
			}
			
			alphabetList.add(entry);
		}
		
		// lower matrix
		for(int outerRowCounter = 1; outerRowCounter < rowCount; outerRowCounter++)
		{
			AlphabetEntry entry = new AlphabetEntry();
			entry.setAlphabetSoupString("");
			entry.setIndices(new ArrayList<RowColumn>());
			
			for(int columnCounter = 0, innerRowCounter = outerRowCounter; 
				columnCounter < columnCount && innerRowCounter < rowCount;
				columnCounter++, innerRowCounter++)
			{
				entry.setAlphabetSoupString(entry.getAlphabetSoupString() + alphabetSoup[innerRowCounter][columnCounter]);
				(entry.getIndices()).add(new RowColumn(innerRowCounter, columnCounter));				
			}
			
			alphabetList.add(entry);
		}
	}
	
	// strings go from lower right to upper left
	private static void constructNortheast()
	{		
		// upper matrix		
		for(int outerColumnCounter = columnCount - 1; outerColumnCounter >= 0; outerColumnCounter--)
		{
			AlphabetEntry entry = new AlphabetEntry();
			entry.setAlphabetSoupString("");
			entry.setIndices(new ArrayList<RowColumn>());
						
			for(int rowCounter = rowCount - 1, innerColumnCounter = outerColumnCounter; 
				rowCounter >= 0 && innerColumnCounter < columnCount;
				rowCounter--, innerColumnCounter++)
			{
				entry.setAlphabetSoupString(entry.getAlphabetSoupString() + alphabetSoup[rowCounter][innerColumnCounter]);
				(entry.getIndices()).add(new RowColumn(rowCounter, innerColumnCounter));				
			}
			
			alphabetList.add(entry);
		}
		
		// lower matrix
		for(int outerRowCounter = 0; outerRowCounter < rowCount - 1; outerRowCounter++)
		{
			AlphabetEntry entry = new AlphabetEntry();
			entry.setAlphabetSoupString("");
			entry.setIndices(new ArrayList<RowColumn>());
			
			for(int columnCounter = 0, innerRowCounter = outerRowCounter; 
				columnCounter < columnCount && innerRowCounter >= 0;
				columnCounter++, innerRowCounter--)
			{
				entry.setAlphabetSoupString(entry.getAlphabetSoupString() + alphabetSoup[innerRowCounter][columnCounter]);
				(entry.getIndices()).add(new RowColumn(innerRowCounter, columnCounter));				
			}
			
			alphabetList.add(entry);
		}
	}

	// strings go from top right to lower left
	private static void constructSouthwest()
	{
		// upper matrix		
		for(int outerColumnCounter = 0; outerColumnCounter < columnCount; outerColumnCounter++)
		{
			AlphabetEntry entry = new AlphabetEntry();
			entry.setAlphabetSoupString("");
			entry.setIndices(new ArrayList<RowColumn>());
						
			for(int rowCounter = 0, innerColumnCounter = outerColumnCounter; 
				rowCounter < rowCount && innerColumnCounter >= 0;
				rowCounter++, innerColumnCounter--)
			{
				entry.setAlphabetSoupString(entry.getAlphabetSoupString() + alphabetSoup[rowCounter][innerColumnCounter]);
				(entry.getIndices()).add(new RowColumn(rowCounter, innerColumnCounter));				
			}
			
			alphabetList.add(entry);
		}
		
		// lower matrix
		for(int outerRowCounter = 1; outerRowCounter < rowCount; outerRowCounter++)
		{
			AlphabetEntry entry = new AlphabetEntry();
			entry.setAlphabetSoupString("");
			entry.setIndices(new ArrayList<RowColumn>());
			
			for(int columnCounter = columnCount - 1, innerRowCounter = outerRowCounter; 
				columnCounter >= 0 && innerRowCounter < rowCount;
				columnCounter--, innerRowCounter++)
			{
				entry.setAlphabetSoupString(entry.getAlphabetSoupString() + alphabetSoup[innerRowCounter][columnCounter]);
				(entry.getIndices()).add(new RowColumn(innerRowCounter, columnCounter));				
			}
			
			alphabetList.add(entry);
		}
	}

	// strings go from lower left to upper right
	private static void constructNorthwest()
	{
		// upper matrix		
		for(int outerColumnCounter = 0; outerColumnCounter < columnCount; outerColumnCounter++)
		{
			AlphabetEntry entry = new AlphabetEntry();
			entry.setAlphabetSoupString("");
			entry.setIndices(new ArrayList<RowColumn>());
						
			for(int rowCounter = rowCount - 1, innerColumnCounter = outerColumnCounter; 
				rowCounter >= 0 && innerColumnCounter >= 0;
				rowCounter--, innerColumnCounter--)
			{
				entry.setAlphabetSoupString(entry.getAlphabetSoupString() + alphabetSoup[rowCounter][innerColumnCounter]);
				(entry.getIndices()).add(new RowColumn(rowCounter, innerColumnCounter));				
			}
			
			alphabetList.add(entry);
		}
		
		// lower matrix
		for(int outerRowCounter = 0; outerRowCounter < rowCount - 1; outerRowCounter++)
		{
			AlphabetEntry entry = new AlphabetEntry();
			entry.setAlphabetSoupString("");
			entry.setIndices(new ArrayList<RowColumn>());
			
			for(int columnCounter = columnCount - 1, innerRowCounter = outerRowCounter; 
				columnCounter >= 0 && innerRowCounter >= 0;
				columnCounter--, innerRowCounter--)
			{
				entry.setAlphabetSoupString(entry.getAlphabetSoupString() + alphabetSoup[innerRowCounter][columnCounter]);
				(entry.getIndices()).add(new RowColumn(innerRowCounter, columnCounter));				
			}
			
			alphabetList.add(entry);
		}
		
	}	
}
