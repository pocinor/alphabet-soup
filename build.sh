#!/bin/bash

if [ `which mvn | wc -l` -eq 0 ]
then
    echo "Please install Maven to build this program."
    exit
fi

if [ $# -eq 0 ]
then 
    mvn clean install
elif [ "$1" == "clean" ]
then
    mvn clean
elif [ "$1" == "test" ]
then
    mvn test
else
    echo "don't know how to do $1"
    exit
fi


