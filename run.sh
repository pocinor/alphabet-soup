#!/bin/bash

if [ $# -ne 1 ]
then
    echo "Please specify an input file."
    exit
fi

java -classpath target/AS-0.0.1-SNAPSHOT.jar alphabetsoup.AlphabetSoup $1
